package com.toddtrowbridge;
// import for user input
import java.util.Scanner;
// import TimeUnit to make arcade style text
import java.util.concurrent.TimeUnit;
// used to generate random int
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // create input scanner
        Scanner inputScanner = new Scanner(System.in);

        // loop the game by default
        boolean loop = true;

        // define user name
        String name;

        // define max int
        int maxInt = 20; // we add 1 later to get 1 to maxInt rather than 0 to maxInt

        // get user name
        slowPrint("\tHello!  What is your name?\n");
        // tab over entered text
        slowPrint("\t");
        // assign user name
        name = getStringFromInput(inputScanner);

        while (loop){
            if (loop){
                loop = gameLoop(inputScanner, name, maxInt);
            }
        }


        // goodbye message
        slowPrint("\tSee you next time! \n");

        // clean up - close input scanner
        inputScanner.close();
    }

    public static boolean gameLoop(Scanner input, String name, int maxInt) {
        int guesses = 1;
        int randomInt = generateRandomIntFromOneTo(maxInt);
        slowPrint("\tLet's play Guess The Number \n");
        // todo comment out below before final push
        slowPrint("\tThe secret number is " + String.valueOf(randomInt) + "\n");
        slowPrint("\tWell, " + name + ", I am thinking of a number between 1 and " + maxInt + "\n");
        slowPrint("\tGuess a number 1 through 20... \n");
        // tab over entered text
        slowPrint("\t");

        // begin game logic
        while(true){
            // display number of guesses remaining
            slowPrint("You have " + getRemainingGuesses(guesses) + " remaining. \n");
            // tab over entered text
            slowPrint("\t");
            // ask user for a number
            int guess = getGuess(input, maxInt);
            // check if guesses are less than 6

            // check number of guesses
            if (guesses == 6){
                slowPrint("\tYou've guessed too many times!  Game over. \n");
                break;
            }

            // too low
            if (guess < randomInt && guesses < 6){
                slowPrint("\tYour guess is too low, try again.\n");
                // tab over entered text
                slowPrint("\t");
                // increment guesses
                guesses++;
            }

            // too high
            if (guess > randomInt && guesses < 6){
                slowPrint("\tYour guess is too high, try again? \n");
                // tab over entered text
                slowPrint("\t");
                // increment guesses
                guesses++;
            }

            // just right
            if (guess == randomInt && guesses < 6) {
                guesses++;
                slowPrint("\tGood job, " + name + "! You guessed my number in " + guesses + " guess(es)! \n");
                break;
            }
        }
        slowPrint("\tWould you like to play again? (y/n)\n");
        String playAgain = getStringFromInput(input);
        // tab over entered text
        slowPrint("\t");
        if (playAgain != "y" || playAgain != "Y"){
            return false;
        }
        return true;
    }

    public static int getRemainingGuesses(int guesses){
        return 7 - guesses;
    }

    public static String getStringFromInput(Scanner input) {
        String answer = "";
        try {
            answer = input.next();
        }
        // catch exception
        catch (Exception e) {
            System.out.println("something broke");
        }
        return answer;
    }

    public static int getIntFromInput(Scanner input){
        int answer = 0;
        try {
            answer = Integer.valueOf(input.next());
        }
        // catch exception
        catch (Exception e) {
            System.out.println("something broke");
        }
        return answer;
    }

    public static int getGuess(Scanner input, int maxInt){
        int newGuess;
        while (true) {
            newGuess = getIntFromInput(input);
            if (newGuess >= 1 && newGuess <= maxInt) {
                slowPrint("\tYou guessed: " + newGuess + "\n");
                return newGuess;
            } else {
                slowPrint("\tReally??? Just type one of the following numbers: \n");
                slowPrint("\t1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 \n");
                // tab over entered text
                slowPrint("\t");
            }
        }
    }

    // use to generate a random int from 1 - input int
    public static int generateRandomIntFromOneTo(int maxInt){
        // generate random int from 1 to 20
        Random rand = new Random();
        int randomInt = rand.nextInt(maxInt);
        return randomInt + 1; // add 1 to get 1 through input int rather than 0 through input int
    }

    // the following method prints to the console with a delay between each character
    // shamelessly stolen from: https://replit.com/talk/learn/Slow-Print-tutorial-for-JAVA/51697
    // since I didn't write this I'm going to break down what I think is happening
    public static void slowPrint(String output) {
        // for loop that runs for each character in the method input string
        for (int i = 0; i < output.length(); i++) {
            // grab the character at position i
            char c = output.charAt(i);
            // print the single character
            System.out.print(c);
            // wrapped in a try catch statement
            try {
                // pause the for loop for X milliseconds
                TimeUnit.MILLISECONDS.sleep(1);
            }
            // catch exception
            catch (Exception e) {
                System.out.println("something broke");
            }
        }
    }
}
